# React SSR

A small repository set up with a very basic React application, to demonstrate how to set up Server Side Rendering and process ES6 modules, that aren't otherwise transpiled.

To really see the benefit of this project, you'll have to edit `node_modules` after installing dependencies, so start with that:

```bash
yarn install
```

Then, within the `node_modules/@material-ui/core/esm` directory, create a folder, `SSRButton`, and paste the following code in it:

```jsx
import React, { useEffect } from 'react';

const Button = (props) => {
    const { onClick, children } = props;
    window.setTimeout(() => {
        document.body.classList.add('ssr-button-test');
    }, 10)

    return (
        <button onClick={onClick} style={{backgroundColor: 'blue', padding: '20px', border: '0'}}>
            SSR Button {children}
        </button>
    )
}
export default Button;

```
 
This is a simple button, but it makes use of `window` and `document`. This isn't a practical component at all, it's only meant to show that SSR will still work even when Browser API's are being referenced.

Now that we have added an ES6 module in `node_modules`, we can call the dev command to run the application, and see SSR in action!

```bash
yarn dev
```

The output in the terminal is a bit noisy, but you'll want to open the following URL in your browser:

```
http://localhost:3000
```

With the application loaded in the browser, use the keyboard command `CTRL` + `U` to view source, and see that the button HTML code is available there!

You'll also notice that the CSS class name **is not** on the `body` element. This is expected, because the document.body element on the server is just mocked. However, the class name **is** added client side (if you use DevTools to view the DOM)
