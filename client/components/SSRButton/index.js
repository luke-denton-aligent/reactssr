import React, { useEffect } from 'react';

// This block demonstrates that the getName() function will be called before the server processes any requirests
// function getName() {
//     return 'Test'
// }
//
// const myData = {
//     name: getName()
// }
//
// console.log({myData});
// process.exit();

// This block will fail, because `window` isn't defined (even though jsdom is being imported..)
// const myData = {
//     name: window.name
// }

// The following will work fine
// if (typeof window !== 'undefined') {
//     console.log({window});
//     process.exit();
// }

const Button = (props) => {
    const { onClick, children } = props;

    window.setTimeout(() => {
        document.body.classList.add('Testing');
    }, 1000)

    return (
        <button onClick={onClick} style={{backgroundColor: 'blue', padding: '20px', border: '0'}}>
            👋 {children}
        </button>
    )
}
export default Button;
