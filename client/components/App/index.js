import React from 'react';
import style from './style.less';
import Button from '../SSRButton'

const App = () => (<>
    <div className={style.app}>Hello World</div>
    <Button onClick={e => alert('Hello You!')}>Say Hello Back!</Button>
</>);

export default App;
